
package com.mycompany.cpit252project;
//Proxy design pattern.
public class authorizationProxy implements Authorization{
     String authorizationCode = "123";
    String userEnteredCode ="";

    private Authorization ta = new TradingAuthorization();

    public authorizationProxy() {
    }
       
    public authorizationProxy(String userEnteredCode) {
        this.userEnteredCode = userEnteredCode;
        
    }

    @Override
    public void tradingAccess() {
        if (userEnteredCode.equalsIgnoreCase(authorizationCode)) {
         ta.tradingAccess();
        }else{

            System.out.println("sorry failed access");
                System.exit(0);
        }
    }

}