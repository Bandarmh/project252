//Command design pattern.
package com.mycompany.cpit252project;


public interface Order {

    void execute();
}

